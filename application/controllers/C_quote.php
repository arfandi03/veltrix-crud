<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class C_quote extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Libarary
        $this->load->library('form_validation');
        // Model
        $this->load->model('M_quote', 'model');
    }

    public function index()
    {
        $data['contents'] = 'contents/quote/index';
        $this->load->view('master', $data);
    }

    public function create()
    {
        $data['contents'] = 'contents/quote/add';
        $data['quote_tnc'] = $this->model->get_quote_tnc();
        $data['quote_tarif'] = $this->model->get_quote_tarif();
        $this->load->view('master', $data);
    }

    public function store()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('name[]', 'Nama Client', 'required');
        $this->form_validation->set_rules('email[]', 'Email Client', 'required');
        $this->form_validation->set_rules('cell_number[]', 'Cell Client', 'required');
        $this->form_validation->set_rules('amount[]', 'Amount', 'required');
        // $this->form_validation->set_rules('contract_terms[]', 'Contracts Terms', 'required');
        $data = 0;
        $temp_message = '';
        $contract_terms = $this->input->post('contract_terms', TRUE);
        foreach ($contract_terms as $term) {
            if ($term != ''){
                $data++;
            }
        }
        if ($data == 0) { 
            $temp_message .= ' You must choose at least one contract_term ';
            $temp_message .= '
';
        }
        // $this->form_validation->set_rules('nama_tarif[]', 'Package List', 'required');
        $data = 0;
        $nama_tarif = $this->input->post('nama_tarif', TRUE);
        foreach ($nama_tarif as $tarif) {
            if ($tarif != ''){
                $data++;
            }
        }
        if ($data == 0) { 
            $temp_message .= ' You must choose at least one package ';
        }
        if ($this->form_validation->run() == FALSE){
            $res['status'] = '400';
            $res['message'] = validation_errors(' ',' ');
            $res['message'] .= $temp_message;
        }else{
            $res = $this->model->insert_contract($post);
        }

        echo json_encode($res);
    }

    public function delete()
    {
        $post = $this->input->post();
        $res = $this->model->delete_contract($post);
        return $this->output
        ->set_content_type('application/json')
        ->set_status_header($res['status'])
        ->set_output(json_encode($res));
    }

    public function detail()
    {
        $id = $this->input->post('id');
        $data['contract'] = $this->model->get_contract_by_id($id);
        $data['details_contract'] = $this->model->get__details_contract_by_id($id);
        $this->load->view('contents/quote/detail', $data);
    }

    public function pdf($id)
    {
        $data['contract'] = $this->model->get_contract_by_id($id);
        $data['details_contract'] = $this->model->get__details_contract_by_id($id);
        $html = $this->load->view('contents/quote/pdf', $data, TRUE);
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function username_check($str)
    {
            if ($str == 'test')
            {
                    $this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
                    return FALSE;
            }
            else
            {
                    return TRUE;
            }
    }
    
    public function get_datatables()
    {
        $post = $this->input->post();

        $params = [
            'offset'        => $post['start'],
            'limit'         => $post['length'],
            'search'        => $post['search']['value'],
        ];

        $params['column_order'] = [''];

        if ($post['order']['0']['column']) {
            $params['order_column'] = $post['order']['0']['column'];
        }

        if ($post['order']['0']['dir']) {
            $params['order_dir'] = $post['order']['0']['dir'];
        }

        $data_contracts = $this->model->get_serverside($params);

        $data = $this->process_datatables($data_contracts);

        echo json_encode($data);
    }

    public function process_datatables($raw_data)
    {
        $data = [];
    
        $data = [
            'data'  => [],
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $raw_data['record_total'],
            'recordsFiltered' => $raw_data['record_filter']
        ];
    
        $no = $this->input->post('start') + 1;
        $key = 0;
    
        foreach ($raw_data['result'] as $k => $v) {
    
            $button = '<button class="btn btn-sm btn-success pdf" data-id="'.$v['id'].'">PDF</button>                
            <button class="btn btn-sm btn-danger delete" data-id="'.$v['id'].' data-id-pp="'.$v['id_users1'].' data-id-pp1="'.$v['id_users2'].'">Delete</button>
            <button class="btn btn-sm btn-primary detail" data-id="'.$v['id'].'">Detail</button>';

            $row = [];
            $row[] = $no++ . '.';
            $row[] = $button;
            $row[] = $v['event_date'];
            $row[] = $v['client_names'];
            $row[] = $v['contract_number'];
            $row[] = $v['client1_info'];
            $row[] = $v['client2_info'];
            $row[] = $v['subtotal'];
            $row[] = $v['tax'];
            $row[] = $v['grandtotal'];
            $data['data'][] = $row;
    
            $key++;
        }
        return $data;
    }
}

/* End of file C_quote.php */

?>