<div class="row align-items-center">
    <div class="col-sm-12">
        <div class="float-right d-none d-md-block">

            <a href="<?= base_url(); ?>quote/pdf/<?= $contract['contract']['id'] ?>" target="_blank" class="btn btn-success btn-sm" style="margin-left: 5px; margin-bottom: 5px">Print</a>
            <a href="#" class="btn btn-primary btn-sm back" style="margin-left: 5px; margin-bottom: 5px">Batal</a>

        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-6">
        <div class="card">
            <h4 class="card-header mt-0">
                Data Contracts
                <!-- <a href="" data-id_contracts="<?= $contract['contract']['id'] ?>" class="btn btn-warning btn-sm edit_contracts" style="margin-left: 5px;">Edit</a> -->
            </h4>
            <div class="card-body">
                
                <dl class="row mb-0">
                    <dt class="col-sm-4">Event Date</dt>
                    <dd class="col-sm-8">: <?= $contract['contract']['event_date'] ?></dd>

                    <dt class="col-sm-4">Contract Number</dt>
                    <dd class="col-sm-8">: <?= $contract['contract']['contract_number'] ?></dd>
                </dl>
                <hr>
                <?php foreach($contract['users'] as $user) : ?>
                <dl class="row mb-0">
                    <dt class="col-sm-4">Full Name</dt>
                    <dd class="col-sm-8">: <?= $user['name'] ?></dd>

                    <dt class="col-sm-4">Cell Number</dt>
                    <dd class="col-sm-8">: <?= $user['cell_number'] ?></dd>

                    <dt class="col-sm-4">Email Address</dt>
                    <dd class="col-sm-8">: <?= $user['email'] ?></dd>
                </dl>
                <br>
                <?php endforeach; ?>
                <hr>

                <dl class="row mb-0">
                    <?php foreach($contract['contracts_payment_plans'] as $payment_plan) : ?>
                    <?php $totalpayment = 0; $totalpayment += $payment_plan['amount']; ?>
                    <dt class="col-sm-3"><?= $payment_plan['payment_sequence'] ?></dt>
                    <dd class="col-sm-4">: $ <?= $payment_plan['amount'] ?></dd>
                    <dt class="col-sm-2">Due</dt>
                    <dd class="col-sm-3">: <?= $payment_plan['due_date'] ?></dd>
                    <?php endforeach; ?>
                </dl>
                <hr>

                <div class="row">
                    <div class="col-sm-4 text-center">
                        <p>Client 1</p>
                        <div class="img1">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=" alt="" height="80px" width="80px">
                        </div>
                        <p><?= $contract['contract']['signature1'] ?></p>
                    </div>

                    <div class="col-sm-4 text-center">
                        <p>Client 2</p>
                        <div class="img2">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=" alt="" height="80px" width="80px">
                        </div>
                        <p><?= $contract['contract']['signature2'] ?></p>
                    </div>

                    <div class="col-sm-4 text-center">
                      <p>Signature 3</p>
                      <div class="img3">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=" alt="" height="80px" width="80px">
                        <p><?= $contract['contract']['signature3'] ?></p>
                      </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <h4 class="card-header mt-0">
                Details Contracts
            </h4>
                
            <div class="card-body">
                <ul>
                    <?php foreach($details_contract as $detail) : ?>
                    <?php $subtotal = 0; $subtotal += $detail['harga']; ?>
                    <b><?= $detail['nama_tarif']; ?></b> - $ <?= $detail['harga']; ?>
                        <?php foreach(explode(",",$detail['isi_paket']) as $paket) :  ?>  
                            <?php if(!empty($paket)): ?>                                                                                         
                            <li><?= $paket; ?></li>
                            <?php else: ?>
                            <br>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            
                <table width="100%" cellspacing="0">

                    <tr>
                        <td class="r-bold font-size-8 border-bottom-0 border-left-1 border-right-0 centerp-left" width="30%">Sub Total</td>
                        <td class=" font-size-8 border-bottom-0 border-left-0 border-right-0 centerp-left" width="70%">: $ <?= $subtotal; ?></td>
                    </tr>
                    <tr>
                        <td class="r-bold font-size-8 border-bottom-0 border-left-1 border-right-0 centerp-left" width="30%">Tax</td>
                        <td class=" font-size-8 border-bottom-0 border-left-0 border-right-0 centerp-left" width="70%">: $ <?php $tax =  $subtotal * 0.0825; echo $tax; ?></td>
                    </tr>
                    <tr>
                        <td class="r-bold font-size-8 border-bottom-0 border-left-1 border-right-0 centerp-left" width="30%">Grand Total</td>
                        <td class=" font-size-8 border-bottom-0 border-left-0 border-right-0 centerp-left" width="70%">: $ <?php $grandtotal =  $subtotal + $tax; echo $grandtotal; ?></td>
                    </tr>

                    <tr>
                        <td class=" font-size-8 border-bottom-0 border-left-1 border-right-0 centerp-left" width="30%">Total Payment</td>
                        <td class=" font-size-8 border-bottom-0 border-left-0 border-right-0 centerp-left" width="70%">: $ <?= $totalpayment; ?></td>
                    </tr>

                    <tr>
                        <td class=" font-size-8 border-bottom-0 border-left-1 border-right-0 centerp-left" width="30%">Balance</td>
                        <td class=" font-size-8 border-bottom-0 border-left-0 border-right-0 centerp-left" width="70%">: $ <?php $balance =  $totalpayment - $grandtotal; echo $balance; ?></td>
                    </tr>

                </table>
                <br>

                <table class="table nowrap">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Tanggal</th>
                            <th>Kode Transaksi</th>
                            <th>No COA</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>

                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <h4 class="card-header mt-0">
                Crews & Expenses          
            </h4>
            <div class="card-body">

                <h4>Crews :</h4>
                <br>
                <hr>
                
                <h4>Expenses :</h4>
                <br>
                <hr>
                
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <h4 class="card-header mt-0">
                Contracts Details & Terms
            </h4>
            <div class="card-body">
                
                <?php foreach($contract['contract_terms'] as $term) : ?>
                <br>
                <b><?= $term['title']; ?></b>
                <br>
                <br>
                <?= $term['contract_term']; ?>
                <br>
                <?php endforeach; ?>
                <hr>
                
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        const class_name = 'quote';

        $('.edit_contracts').click(function(e) {
            e.preventDefault()
            $.ajax({
                url : "<?= base_url(); ?>" + class_name + "/edit",
                type: "POST",
                data: {
                    id_contracts: $(this).data('id_contracts'),
                    type: 'edit-data-contracts'
                },
                success: (html) => {
                    $(".load").html('');
                    $(".load").html(html);
                },
                error: (e) => {
                    alert(`${e.status} - ${e.statusText}`);
                }
            });
        });

        $('.back').on('click', function(e) {
            e.preventDefault();
            location.reload();
        });
    });
</script>