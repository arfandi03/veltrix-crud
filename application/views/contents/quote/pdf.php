<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    *,
    ::after,
    ::before {
        box-sizing: border-box;
    }

    body {
        padding: 0;
        margin: 0;
    }

    .content {
        padding: 20px;
    }

    .row {
        margin-top: 20px;
        display: flex;
        justify-content: center;
    }

    .col {
        position: relative;
        width: 100%;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
    }

    .card {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
    }

    .card-header {
        padding: .75rem 1.25rem;
        margin-bottom: 0;
        background-color: rgba(0, 0, 0, .03);
        border-bottom: 1px solid rgba(0, 0, 0, .125);
    }

    .card-header:first-child {
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }

    .card-body {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }

    .table {
        /* display: flex; */
        /* justify-content: start; */
        text-align: left;
        width: 100%;
        max-width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
    }

    .table td,
    .table th {
        padding: .75rem;
        vertical-align: top;
    }

    .table th {
        width: 150px;
        text-align: left;
    }

    .table-bordered {
        width: 100%;
        max-width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered td,
    .table-bordered th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #acacac;
    }

    .ttd {
        position: relative;
        width: 100%;
    }

    .ttd .content {
        display: inline-block;
        width: 33%;
        height: fit-content;
        /* margin: auto; */
        margin: 0 auto;
        text-align: center;
    }
</style>

<body>
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Data Contracts
                    </div>
                    <div class="card-body">
                        <table class="table" style="margin-top: 20px;">
                            <tr>
                                <th>Even Date</th>
                                <td>: <?= $contract['contract']['event_date'] ?></td>
                            </tr>
                            <tr>
                                <th>Contact Number</th>
                                <td>: <?= $contract['contract']['contract_number'] ?></td>
                            </tr>
                        </table>
                        <hr>
                        <table class="table">
                            <?php foreach ($contract['users'] as $user) : ?>
                                <!-- <th></th> -->
                                <tr>
                                    <th>Full Name</th>
                                    <td>: <?= $user['name'] ?></td>
                                </tr>
                                <tr>
                                    <th>Call Number</th>
                                    <td>: <?= $user['cell_number'] ?></td>
                                </tr>
                                <tr>
                                    <th>Email Address</th>
                                    <td>: <?= $user['email'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <hr>
                        <table class="table">
                            <?php foreach ($contract['contracts_payment_plans'] as $payment_plan) : ?>
                                <?php $totalpayment = 0;
                                $totalpayment += $payment_plan['amount']; ?>
                                <tr>
                                    <th><?= $payment_plan['payment_sequence'] ?></th>
                                    <td>: $ <?= $payment_plan['amount'] ?></td>
                                    <th style="width: 100px !important;">Due</th>
                                    <td>: <?= $payment_plan['due_date'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <hr>
                        <table class="ttd">
                            <tr style="height: 100px !important;">
                                <th>
                                    <div class="content">
                                        <h5>Client 1</h5>
                                        <!-- <img src="" width="80px" height="80px" alt=""> -->
                                        <br><br><br><br><br><br><br>
                                        <p><?= $contract['contract']['signature1'] ?></p>
                                        <br><br><br>
                                    </div>
                                </th>
                                <th>
                                    <div class="content">
                                        <h5>Client 2</h5>
                                        <!-- <img src="" width="80px" height="80px" alt=""> -->
                                        <br><br><br><br><br><br><br>
                                        <p><?= $contract['contract']['signature2'] ?></p>
                                        <br><br><br>
                                    </div>
                                </th>
                                <th>
                                    <div class="content">
                                        <h5>Client 3</h5>
                                        <!-- <img src="" width="80px" height="80px" alt=""> -->
                                        <br><br><br><br><br><br><br>
                                        <p><?= $contract['contract']['signature3'] ?></p>
                                        <br><br><br>
                                    </div>
                                </th>
                            </tr>
                        </table>
                        <!-- <div class="ttd">
                            <div class="content">
                                <h5>Client 1</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature1'] ?></p>
                            </div>
                            <div class="content">
                                <h5>Client 2</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature2'] ?></p>
                            </div>
                            <div class="content">
                                <h5>Client 3</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature3'] ?></p>
                            </div>
                        </div> -->
                        <!-- <div class="row">
                            <div class="col">
                                <h5>Client 1</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature1'] ?></p>
                            </div>
                            <div class="col">
                                <h5>Client 2</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature2'] ?></p>
                            </div>
                            <div class="col">
                                <h5>Client 3</h5>
                                <img src="" width="80px" height="80px" alt="">
                                <p><?= $contract['contract']['signature3'] ?></p>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Details Contracts
                    </div>
                    <div class="card-body">
                        <?php foreach ($details_contract as $detail) : ?>
                            <?php $subtotal = 0;
                            $subtotal += $detail['harga']; ?>
                            <p style="margin-top: 40px;"><b><?= $detail['nama_tarif']; ?></b> - $ <?= $detail['harga']; ?></p>
                            <ul>
                                <?php foreach (explode(",", $detail['isi_paket']) as $paket) :  ?>
                                    <?php if (!empty($paket)) : ?>
                                        <li>
                                            <?= $paket; ?>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        <?php endforeach; ?>
                        <table class="table">
                            <tr>
                                <th>Subtotal</th>
                                <td>: $ <?= $subtotal; ?></td>
                            </tr>
                            <tr>
                                <th>TAX</th>
                                <td>: $ <?php $tax =  $subtotal * 0.0825;
                                        echo $tax; ?></td>
                            </tr>
                            <tr>
                                <th>Grand Total</th>
                                <td>: $ <?php $grandtotal =  $subtotal + $tax;
                                        echo $grandtotal; ?></td>
                            </tr>
                            <tr>
                                <th>Total Payment</th>
                                <td>: $ <?= $totalpayment; ?></td>
                            </tr>
                            <tr>
                                <th>Balance</th>
                                <td>: $ <?php $balance =  $totalpayment - $grandtotal;
                                        echo $balance; ?></td>
                            </tr>
                        </table>
                        <table class="table-bordered">
                            <tr>
                                <th>Tanggal</th>
                                <th>Kode Transaksi</th>
                                <th>No COA</th>
                                <th>Jumlah</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Crews & Expenses
                    </div>
                    <div class="card-body">
                        <h4 style="margin-top: 40px;">Crews :</h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima dolore cumque eveniet dolores
                            nesciunt. Velit itaque id nulla soluta sapiente vel dicta quaerat tempore atque, dolorum
                            similique consequuntur hic necessitatibus.</p>
                        <br>
                        <h4>Expenses :</h4>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quaerat molestiae necessitatibus
                            tempore at accusamus iste, quos saepe dolorem recusandae odit perferendis autem voluptatem
                            consequatur laudantium doloribus. Repellat nam eos obcaecati?</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Contracts Details & Terms
                    </div>
                    <div class="card-body">
                        <?php foreach ($contract['contract_terms'] as $term) : ?>
                            <h4 style="margin-top: 40px;"><?= $term['title']; ?></h4>
                            <?= $term['contract_term']; ?>
                            <br>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>