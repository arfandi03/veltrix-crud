<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <div class="float-right d-none d-md-block">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="load">
                        <h4 class="card-title">Contract List</h4>
                        <br>
                        <div class="table-responsive">
                            <table id="datatable-quote" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th></th>
                                        <th>Event Date</th>
                                        <th>Client Names</th>
                                        <th>Contract Number</th>
                                        <th>Nama-Email-Cell Number-Client 1</th>
                                        <th>Nama-Email-Cell Number-Client 2</th>
                                        <th>Subtotal Fee</th>
                                        <th>Tax</th>
                                        <th>Grand Total</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
</div> 
<!-- container-fluid -->

<script>
    $(document).ready(function () {

        const class_name = 'quote';
        const initiateDatatables = () => {
        table = $("#datatable-quote").DataTable({
            "lengthMenu": [10, 50, 100, 200],
            "columnDefs": [{
                "targets": [0],
                "orderable": false,
            }],
            "scrollX": true,
            "pageLength": 10,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "info": true,
            "searchDelay": 750,
            "ajax": {
                "url": '<?= base_url('quote/datatable'); ?>',
                "type": 'POST'
            }
        });
        }
    
        initiateDatatables();

        $(document).on('click', '.pdf', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            aktif_pdf = window.open('<?= base_url(); ?>' + class_name + '/pdf/' + id);
        });

        $(document).on('click', '.delete', function(e) {
            let id_clients1 = $(this).data('id-pp');
            let id_clients2 = $(this).data('id-pp1');
            e.preventDefault();
            var r = confirm("Yakin hapus data ini ?");
            if (r == true) {
                $.ajax({
                    url : "<?= base_url(); ?>" + class_name + "/delete",
                    type: "POST",
                    data: {
                        id: $(this).data('id'),
                        id1: id_clients1,
                        id2: id_clients2
                    },
                    success: (res) => {
                        alert(res.message);
                        if (res.status == '200') {
                            window.location.href = "<?= base_url('quote') ?>";
                        } else {
                            return false;
                        }
                    },
                    error: (e) => {
                        alert(`${e.status} - ${e.statusText}`);
                        window.location.href = "<?= base_url('quote') ?>";
                    }
                });
            } else {
                return false;
            }
        });

        $(document).on('click', '.detail', function(e) {
            e.preventDefault();
            $.ajax({
                url : "<?= base_url(); ?>" + class_name + "/detail",
                type: "POST",
                data: {
                    id: $(this).data('id')
                },
                success: (html) => {
                    $(".load").html(html);
                },
                error: (e) => {
                    alert(`${e.status} - ${e.statusText}`);
                }
            });
        });
    
    });
</script>
