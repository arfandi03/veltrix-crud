<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Dashboard | Veltrix - Admin & Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
        <meta content="Themesbrand" name="author">

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.ico">

        <link href="<?= base_url(); ?>assets/libs/chartist/chartist.min.css" rel="stylesheet">

        <!-- JAVASCRIPT -->
        <script src="<?= base_url(); ?>assets/libs/jquery/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/node-waves/waves.min.js"></script>

        <!-- Bootstrap Css -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">
        <!-- Icons Css -->
        <link href="<?= base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css">
        <!-- App Css-->
        <link href="<?= base_url(); ?>assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css">
        <!-- Moment.js -->
        <script type="text/javascript" src="<?= base_url(); ?>assets/libs/moment/moment.js"></script>

        <!-- datetimepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

        <!-- daterangepicker -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" /> -->

        <!-- autonumeric -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/1.9.46/autoNumeric.min.js"></script>

        <!-- Select2 -->
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/select2/css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/select2/select2-bootstrap.css" />

        <!-- Select2 -->
        <script type="text/javascript" src="<?= base_url(); ?>assets/select2/js/select2.min.js"></script>

        <!-- JQuery Masked -->
        <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.mask.js"></script>

        <!-- DATATABLES -->
        <link href="<?= base_url(); ?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="<?= base_url(); ?>assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Required datatable js -->
        <script src="<?= base_url(); ?>assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

        <!-- Buttons examples -->
        <script src="<?= base_url(); ?>assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/jszip/jszip.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/pdfmake/build/vfs_fonts.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>

        <!-- Responsive examples -->
        <script src="<?= base_url(); ?>assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <!-- Peity chart-->
        <script src="<?= base_url(); ?>assets/libs/peity/jquery.peity.min.js"></script>

        <!-- Plugin Js-->
        <script src="<?= base_url(); ?>assets/libs/chartist/chartist.min.js"></script>
        <script src="<?= base_url(); ?>assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min.js"></script>

        <script src="<?= base_url(); ?>assets/js/pages/dashboard.init.js"></script>

        <script src="<?= base_url(); ?>assets/js/app.js"></script>

        <!-- Datatable init js -->
        <script src="<?= base_url(); ?>assets/js/pages/datatables.init.js"></script> 

    </head>
    <body data-sidebar="dark">

        <div id="layout-wrapper">

        <?php $this->load->view('layout/header'); ?>

        <?php $this->load->view('layout/left_sidebar'); ?>

        <div class="main-content">
            <div class="page-content">
                <?php $this->load->view($contents); ?>
            </div>
        </div>


        </div>


        <?php $this->load->view('layout/right_sidebar'); ?>

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
    </body>
</html>