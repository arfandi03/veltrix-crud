<?php

class App_encryption
{

	public function encrypt($data)
	{
		$CI = &get_instance();
		$CI->encryption->initialize(
			array(
				'cipher'	=> 'aes-256',
				'mode' 		=> 'ctr',
				'key' 		=> $CI->config->item('encryption_key')
			)
		);

		$res = $CI->encryption->encrypt($data);

		return $res;
	}

	public function decrypt($data)
	{
		$CI = &get_instance();
		$CI->encryption->initialize(
			array(
				'cipher'	=> 'aes-256',
				'mode' 		=> 'ctr',
				'key' 		=> $CI->config->item('encryption_key')
			)
		);

		$res = $CI->encryption->decrypt($data);

		return $res;
	}
}
