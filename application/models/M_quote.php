<?php

class M_quote extends CI_Model {

    public function insert_contract($post)
    {
        $user_id = $this->insert_user($post);

        if($user_id){
            $contract_terms = [];
            foreach($post['contract_terms'] as $key => $term){
                if(!empty($term)){
                    $contract_term['title'] = $post['title_master_contract_terms'][$key];
                    $contract_term['contract_term'] = $term;
                    array_push($contract_terms, $contract_term);
                }
            }

            $data_contract = [
                'event_date' => $post['event_date'],
                'id_users1' => $user_id[0],
                'id_users2' => $user_id[1],
                'nama1' => $post['name'][0],
                'nama2' => $post['name'][1],
                'contract_terms' => json_encode($contract_terms)
            ];
            $insert = $this->db->insert('cp_contracts', $data_contract);
            
            if($insert){
                $id_contract = $this->db->insert_id();
                $data_contract = [
                    'contract_number' => 'C/'.date('y').'/'.$id_contract
                ];
    
                $this->db->update('cp_contracts', $data_contract, ['id' => $id_contract]);
            }else{
                $res = [
                    'status' => '400',
                    'message' => 'Gagal insert contract'
                ];
                return $res;
            }
        }else{
            $res = [
                'status' => '400',
                'message' => 'Gagal insert user'
            ];
            return $res;
        }

        if($insert){
            $insert = $this->insert_contract_detail($post, $id_contract);
        }else{
            $res = [
                'status' => '400',
                'message' => 'Gagal insert contract detail'
            ];
            return $res;
        }

        if($insert){
            $insert = $this->insert_contracts_package($post, $id_contract);
        }else{
            $res = [
                'status' => '400',
                'message' => 'Gagal insert contract package'
            ];
            return $res;
        }

        if($insert){
            $insert = $this->insert_contracts_payment_plan($post, $id_contract);
        }else{
            $res = [
                'status' => '400',
                'message' => 'Gagal insert contract payment plan'
            ];
            return $res;
        }

        if($insert){
            $res = [
                'status' => '200',
                'message' => 'Berhasil insert'
            ];
        }

        return $res;
        
    }

    public function insert_user($post)
    {
        $user_id = [];
        $insert = true;
        foreach($post['name'] as $key => $name){
            if(!empty($name) && $insert){

                $data_user = [
                    'name' => $name,
                    'email' => $post['email'][$key],
                    'cell_number' => $post['cell_number'][$key],
                    'role' => 1
                ];

                $insert = $this->db->insert('cp_users', $data_user);

                array_push($user_id, $this->db->insert_id());
            }
        }

        if($insert){
            return $user_id;
        }

        return $insert;

    }

    public function insert_contract_detail($post, $id_contract)
    {
        $insert = true;
        foreach($post['nama_tarif'] as $key => $nama_tarif){
            if(!empty($nama_tarif) && $insert){
                $data_contract_detail= [
                    'id_contracts' => $id_contract,
                    'id_master_tarif' => $post['id_tarif'][$key],
                    'nama_tarif' => $nama_tarif,
                    'isi_paket' => $post['isi_paket'][$key],
                    'harga' => $post['harga_tarif'][$key],
                    'group' => $post['group'][$key]
                ];

                $insert = $this->db->insert('cp_contracts_details', $data_contract_detail);
            }
        }

        return $insert;
    }

    public function insert_contracts_package($post, $id_contract)
    {
        $insert = true;
        foreach($post['isi_paket'] as $key => $isi_paket){
            if(!empty($isi_paket)){
                foreach(explode(",", $isi_paket) as $paket){
                    if($insert){
                        $data_contracts_package = [
                            'id_contract' => $id_contract,
                            'nama_tarif' => $post['nama_tarif'][$key],
                            'isi_paket_to_do' => $paket,
                            'status' => 'PENDING',
                        ];
    
                        $insert = $this->db->insert('cp_contracts_package', $data_contracts_package);
                    }
                }
            }
        }

        return $insert;
    }

    public function insert_contracts_payment_plan($post, $id_contract)
    {
        $sequence = 1;
        $insert = true;
        foreach($post['amount'] as $key => $amount){
            if(!empty($amount) && $insert){

                $data_contracts_payment_plan = [
                    'id_contracts' => $id_contract,
                    'contract_number' => 'C/'.date('y').'/'.$id_contract,
                    'amount' => $amount,
                    'due_date' => $post['due_date'][$key],
                    'payment_sequence' => $sequence,
                    'desc' => $post['desc'][$key]
                ];

                $insert = $this->db->insert('cp_contracts_payment_plans', $data_contracts_payment_plan);
            }
            $sequence++;
        }

        return $insert;
    }

    public function delete_contract($post)
    {
        $data_contract = [
            'del_date' => date('Y-m-d H:i:s')
        ];

        $delete = $this->db->update('cp_contracts', $data_contract, ['id' => $post['id']]);

        if($delete){
            $this->db->delete('cp_users', array('id' => $post['id1']));
            $this->db->delete('cp_users', array('id' => $post['id2']));
            $res = [
                'status' => '200',
                'message' => 'Berhasil delete contract'
            ];
        }else{
            $res = [
                'status' => '400',
                'message' => 'Gagal delete contract'
            ];
        }

        return $res;

    }

    public function get_quote_tnc()
    {
        $this->db->select('*');
        $this->db->from('cp_master_quote_tnc');
        return $this->db->get()->result_array();
    }

    public function get_quote_tarif()
    {
        $this->db->select('group');
        $this->db->from('cp_master_quote_tarif');
        $this->db->group_by("group");
        $result['promos'] = $this->db->get()->result_array();

        foreach($result['promos'] as $promo){
            $this->db->select('*');
            $this->db->from('cp_master_quote_tarif');
            $this->db->where('group', $promo['group']);
            $result[$promo['group']] = $this->db->get()->result_array();
        }

        return $result;
    }

    public function get_contract_by_id($id)
    {
        $this->db->select("*");
        $this->db->from('cp_contracts');
        $this->db->where('id', $id);
        $result['contract'] = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('cp_users');
        $this->db->where_in('id', [ $result['contract']['id_users1'], $result['contract']['id_users2'] ]);
        $result['users'] = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('cp_contracts_payment_plans');
        $this->db->where('id_contracts', $id);
        $result['contracts_payment_plans'] = $this->db->get()->result_array();

        $result['contract_terms'] = json_decode($result['contract']['contract_terms'], TRUE);

        return $result;
    }

    public function get__details_contract_by_id($id)
    {
        $this->db->select("*");
        $this->db->from('cp_contracts_details');
        $this->db->where('id_contracts', $id);
        return $this->db->get()->result_array();
    }

    public function get_serverside($params)
    {
    
        $params['column_order']   = array('event_date', 'contract_number' , 'client1_info', 'client2_info', 'subtotal', 'tax', 'grandtotal');
    
        $params['column_search']  = array('event_date', 'contract_number');
    
        $params['order']          = array('id' => 'desc');
    
        $this->db->select("
            a.id,
            a.id_users1,
            a.id_users2,
            a.event_date,
            CONCAT(c.name, ' & ', d.name) as client_names,
            a.contract_number,
            CONCAT(c.name, '-', c.email, '-', c.cell_number) as client1_info,
            CONCAT(d.name, '-', d.email, '-', d.cell_number) as client2_info,
            sum(b.harga) as subtotal,
            sum(b.harga) * 0.0825 as tax,
            (sum(b.harga) * 0.0825) + sum(b.harga) as grandtotal
        ");
    
        $this->db->from('cp_contracts a');
        $this->db->join('cp_contracts_details b', 'b.id_contracts = a.id', 'left');
        $this->db->join('cp_users c', 'c.id = a.id_users1', 'left');
        $this->db->join('cp_users d', 'd.id = a.id_users2', 'left');
        $this->db->group_by("b.id_contracts");
        $this->db->where('a.del_date', null);
    
        if (!empty($params['id'])) {
            $this->db->where('a.id', $params['id']);
        }

        if (!isset($params['offset'])) {
            $params['offset'] = '0';
        }
        if (!empty($params['limit'])) {
            $this->db->limit($params['limit'], $params['offset']);
        }
    
        $this->_get_datatables_query($params);
    
        $result = $this->db->get()->result_array();
    
        $res = [
            'result'        => $result,
            'record_total'  => $this->get_total_serverside($params),
            'record_filter' => $this->get_filtered_serverside($params)
        ];
    
        return $res;
    }
 
    private function _get_datatables_query($params)
    {
        $i = 0;
    
        if (!empty($params['search'])) {
            $this->db->group_start();
            foreach ($params['column_search'] as $item) {
                if ($i == 0) {
                    $this->db->like($item, $params['search']);
                } else {
                    $this->db->or_like($item, $params['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }
    
        if (isset($params['order_column'])) {
            if ($params['order_column'] == 0) {
                $column_order_ui = 0;
            } else {
                $column_order_ui = $params['order_column'] - 2;
            }
        }
    
        if (isset($params['order_column'])) {
            $this->db->order_by($params['column_order'][$column_order_ui], $params['order_dir']);
        } else {
            $this->db->order_by(key($params['order']), $params['order'][key($params['order'])]);
        }
    }
 
    public function get_total_serverside($params)
    {
        $this->db->select("COUNT(a.id) total");
    
        $this->db->from('cp_contracts a');
        $this->db->where('a.del_date', null);
        
        if (!empty($params['id'])) {
            $this->db->where('a.id', $params['id']);
        }
    
        return $this->db->get()->row_array()['total'];
    }
    
    public function get_filtered_serverside($params)
    {    
        $this->db->select("COUNT(a.id) total");
    
        $this->db->from('cp_contracts a');
        $this->db->where('a.del_date', null);
    
        if (!empty($params['id'])) {
            $this->db->where('a.id', $params['id']);
        }
    
        if (!empty($params['search'])) {
            $i = 0;
            $this->db->group_start();
            foreach ($params['column_search'] as $item) {
                if ($i == 0) {
                    $this->db->like($item, $params['search']);
                } else {
                    $this->db->or_like($item, $params['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }
    
        return $this->db->get()->row_array()['total'];
    
    }

}

/* End of file M_quote.php */

?>